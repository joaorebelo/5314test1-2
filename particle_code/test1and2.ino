// This #include statement was automatically added by the Particle IDE.
#include <InternetButton.h>

#include "InternetButton.h"
InternetButton b = InternetButton();

 int sequenceBtn[] = {1, 2, 3, 4};
 int nextToClick = 0;

  /*
 btn 1 = led (11 and 1)
 btn 2 = led (2 and 3 and 4)
 btn 3 = led (5 and 6 and 7)
 btn 4 = led (8 and 9 and 10)
 */
 int red[] = {11,1};
 int blue[] = {2,3,4};
 int yellow[] = {5, 6, 7};
 int green[] = {8, 9, 10};
 int dificulty = 0;
 
void setup() {
    b.begin();
    
    Particle.function("restartG", restartGame);
    //load 
    initialLoad();
}

void loop() {
    
    //restart game
    if( (b.buttonOn(1) && b.buttonOn(2)) || (b.buttonOn(1) && b.buttonOn(3)) || (b.buttonOn(1) && b.buttonOn(4)) || (b.buttonOn(2) && b.buttonOn(3)) || (b.buttonOn(2) && b.buttonOn(4)) || (b.buttonOn(3) && b.buttonOn(4)) ){
        Particle.publish("broadcastMessage", "Restart", 60, PUBLIC);
        delay(500);
    }
   
   //button 1 click
   if(b.buttonOn(1)){
       turnLeds(11, 1, 255, 0, 0);
        if(sequenceBtn[nextToClick] == 1){
            nextToClick++;

            if(nextToClick == 4){//Win
            Particle.publish("broadcastMessage", "Win", 60, PUBLIC);
            delay(500);
            }
        }
        else{//lost
            Particle.publish("broadcastMessage", "Lost", 60, PUBLIC);
            delay(500);
        }
    }
    
    //button 2 click
    if(b.buttonOn(2)){
        turn3Leds(2,3,4, 0, 0, 255);
        delay(500);
        if(sequenceBtn[nextToClick] == 2){
            nextToClick++;

            if(nextToClick == 4){//Win
            Particle.publish("broadcastMessage", "Win", 60, PUBLIC);
            delay(500);
            }
        }
        else{//lost
            Particle.publish("broadcastMessage", "Lost", 60, PUBLIC);
            delay(500);
        }
    }
    
    //button 3 click
    if(b.buttonOn(3)){
        turn3Leds(5, 6, 7, 255, 255, 0);
        if(sequenceBtn[nextToClick] == 3){
            nextToClick++;

            if(nextToClick == 4){//Win
            Particle.publish("broadcastMessage", "Win", 60, PUBLIC);
            delay(500);
            }
        }
        else{//lost
            Particle.publish("broadcastMessage", "Lost", 60, PUBLIC);
            delay(500);
        }
    }
    
    //button 4 click
    if(b.buttonOn(4)){
        turn3Leds(8, 9, 10, 0, 255, 0);
        if(sequenceBtn[nextToClick] == 4){
            nextToClick++;

            if(nextToClick == 4){//Win
            Particle.publish("broadcastMessage", "Win", 60, PUBLIC);
            delay(500);
            }
        }
        else{//lost
            Particle.publish("broadcastMessage", "Lost", 60, PUBLIC);
            delay(500);
        }
    }
}

//change 2 led's state
int turnLeds(int led1, int led2, int red, int green, int blue){
    b.ledOn(led1, red, green, blue);
    b.ledOn(led2, red, green, blue); 
    delay(500);
    b.ledOff(led1);
    b.ledOff(led2);
}

//change 3 led's state
int turn3Leds(int led1, int led2, int led3, int red, int green, int blue){
    b.ledOn(led1, red, green, blue);
    b.ledOn(led2, red, green, blue); 
    b.ledOn(led3, red, green, blue); 
    delay(500);
    b.ledOff(led1);
    b.ledOff(led2);
    b.ledOff(led3);
}

//restart game
int restartGame(String command){
    Particle.publish("command ", command);
    
    //pos 0
    int cIndex = command.indexOf(",");
    String um = command.substring(0,cIndex);
    Particle.publish("pos 0 ", um);
    String str = command.substring(cIndex+1);
    
    //pos 1
    cIndex = str.indexOf(",");
    String dois = str.substring(0,cIndex);
    Particle.publish("pos 1 ", dois);
    String str2 = str.substring(cIndex+1);
    
    //pos 2
    cIndex = str2.indexOf(",");
    String tres = str2.substring(0,cIndex);
    Particle.publish("pos 2 ", tres);
    String str3 = str2.substring(cIndex+1);
    
    //pos 3
    cIndex = str3.indexOf(",");
    String quatro = str3.substring(0,cIndex);
    Particle.publish("pos 3 ", quatro);
    String str4 = str3.substring(cIndex+1);
    
    //pos difficulty
    cIndex = str4.indexOf(",");
    String dif = str4.substring(0,cIndex);
    Particle.publish("dificulty ", dif);
    
    sequenceBtn[0] = atoi(um.c_str());
    sequenceBtn[1] = atoi(dois.c_str());
    sequenceBtn[2] = atoi(tres.c_str());
    sequenceBtn[3] = atoi(quatro.c_str());
    dificulty = atoi(dif.c_str());
    
    
    showSequence();
    
    //invert sequence
    if(dificulty == 1){
        int aux = sequenceBtn[0];
        sequenceBtn[0] = sequenceBtn[3];
        sequenceBtn[3] = aux;
        
        aux = sequenceBtn[1];
        sequenceBtn[1] = sequenceBtn[2];
        sequenceBtn[2] = aux;
    }
    delay(500);
}

void showSequence(){
    b.allLedsOn(0,20,20);
    delay(100);
    b.allLedsOff();
    delay(100);
    
    int vR = 255;
    int vG = 0;
    int vB = 0;
    nextToClick = 0;
    for(int i = 0; i < 4; i++) {
        if(sequenceBtn[i] == 1){
            b.ledOn(red[0], 255, 0, 0);
            b.ledOn(red[1], 255, 0, 0); 
            if(dificulty == 0){delay(1000);}else{delay(500);}
            b.ledOff(red[0]);
            b.ledOff(red[1]);
            delay(300);
        }else if(sequenceBtn[i] == 2){
            b.ledOn(blue[0], 0, 0, 255);
            b.ledOn(blue[1], 0, 0, 255);
            b.ledOn(blue[2], 0, 0, 255);
            delay(300);
            if(dificulty == 0){delay(1000);}else{delay(500);}
            b.ledOff(blue[0]);
            b.ledOff(blue[1]);
            b.ledOff(blue[2]);
            delay(300);
        }else if(sequenceBtn[i] == 3){
            b.ledOn(yellow[0], 255, 255, 0);
            b.ledOn(yellow[1], 255, 255, 0);
            b.ledOn(yellow[2], 255, 255, 0);
            if(dificulty == 0){delay(1000);}else{delay(500);}
            b.ledOff(yellow[0]);
            b.ledOff(yellow[1]);
            b.ledOff(yellow[2]);
            delay(300);
        }else if(sequenceBtn[i] == 4){
            b.ledOn(green[0], 0, 255, 0);
            b.ledOn(green[1], 0, 255, 0);
            b.ledOn(green[2], 0, 255, 0);
            if(dificulty == 0){delay(1000);}else{delay(500);}
            b.ledOff(green[0]);
            b.ledOff(green[1]);
            b.ledOff(green[2]);
            delay(300);
        }
        
    }
    
    delay(1000);
    b.allLedsOff();
}

void initialLoad(){
    //all leds
    for(int i = 0; i < 3; i++) {
        b.allLedsOn(0,20,20);
        delay(100);
        b.allLedsOff();
        delay(100);
    }
    //5-note sound
    b.playSong("E5,8,G5,8,E6,8,C6,8,D6,8");
    
    b.allLedsOff();
    delay(1000);
    
    b.ledOn(1, 255, 0, 0);
    b.ledOn(11, 255, 0, 0);
    delay(500);
}