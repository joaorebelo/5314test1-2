package com.example.joorebelo.test1and2;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import io.particle.android.sdk.cloud.ParticleCloud;
import io.particle.android.sdk.cloud.ParticleCloudSDK;
import io.particle.android.sdk.cloud.ParticleDevice;
import io.particle.android.sdk.cloud.ParticleEvent;
import io.particle.android.sdk.cloud.ParticleEventHandler;
import io.particle.android.sdk.cloud.exceptions.ParticleCloudException;
import io.particle.android.sdk.utils.Async;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    TextView txtTitle, txtStarted, txtSequence;
    Button btnNewGame, btnRestartGame, btnDifficultyEasy, btnDifficultyHard;
    EditText edtName;
    String userName = "";
    String difficulty = "";
    String command = "";
    String commandS = "";

    //region account
    // MARK: Particle Account Info
    private final String TAG="Joao";
    private final String PARTICLE_USERNAME = "joao.rebelo92@gmail.com";
    private final String PARTICLE_PASSWORD = "QWEparticle1";

    // MARK: Particle device-specific info
    private final String DEVICE_ID = "340036000f47363333343437";
    //endregion

    // MARK: Particle Publish / Subscribe variables
    private long subscriptionId;

    // MARK: Particle device
    private ParticleDevice mDevice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtTitle = findViewById(R.id.txtTitle);
        txtStarted = findViewById(R.id.txtStarted);
        txtSequence = findViewById(R.id.txtSequence);

        edtName = findViewById(R.id.edtName);

        btnNewGame = findViewById(R.id.btnNewGame);
        btnNewGame.setOnClickListener(this);

        btnRestartGame = findViewById(R.id.btnRestartGame);
        btnRestartGame.setOnClickListener(this);

        btnDifficultyEasy = findViewById(R.id.btnDifficultyEasy);
        btnDifficultyEasy.setOnClickListener(this);

        btnDifficultyHard = findViewById(R.id.btnDifficultyHard);
        btnDifficultyHard.setOnClickListener(this);

        SharedPreferences sp = getSharedPreferences("com.example.joorebelo.test1and2.shared", Context.MODE_PRIVATE);
        userName = sp.getString("userName", "");
        Toast.makeText(getApplicationContext(), "userName: "+ userName, Toast.LENGTH_LONG).show();
        if (userName.equals("")){
            txtStarted.setText("Insert Your name");
        }else {
            txtStarted.setText("Hello " + userName + "!");
            edtName.setVisibility(View.INVISIBLE);
        }

        btnRestartGame.setVisibility(View.INVISIBLE);
        btnDifficultyEasy.setVisibility(View.INVISIBLE);
        btnDifficultyHard.setVisibility(View.INVISIBLE);
        txtSequence.setVisibility(View.INVISIBLE);

        // 1. Initialize your connection to the Particle API
        ParticleCloudSDK.init(this.getApplicationContext());

        // 2. Setup your device variable
        getDeviceFromCloud();

    }

    //Custom function to connect to the Particle Cloud and get the device
    public void getDeviceFromCloud() {
        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {

            @Override
            public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {
                particleCloud.logIn(PARTICLE_USERNAME, PARTICLE_PASSWORD);
                mDevice = particleCloud.getDevice(DEVICE_ID);
                return -1;
            }

            @Override
            public void onSuccess(Object o) {
                Log.d(TAG, "Successfully got device from Cloud");
            }

            @Override
            public void onFailure(ParticleCloudException exception) {
                Log.d(TAG, exception.getBestMessage());
            }
        });
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == btnNewGame.getId()){
            if (edtName.getText().toString().equals("") && userName.equals("")){
                Toast.makeText(getApplicationContext(), "You need to insert a name!", Toast.LENGTH_SHORT).show();
            }else{
                if (userName.equals("")){
                    SharedPreferences sp = getSharedPreferences("com.example.joorebelo.test1and2.shared", Context.MODE_PRIVATE);
                    SharedPreferences.Editor edit = sp.edit();
                    edit.putString("userName", edtName.getText().toString());
                    edit.commit();
                    userName = edtName.getText().toString();
                }

                edtName.setVisibility(View.INVISIBLE);
                btnNewGame.setVisibility(View.INVISIBLE);
                txtStarted.setText("Hello " + userName);
                btnRestartGame.setVisibility(View.VISIBLE);
                txtSequence.setVisibility(View.VISIBLE);
                txtSequence.setText("Choose difficulty:");
                btnDifficultyEasy.setVisibility(View.VISIBLE);
                btnDifficultyHard.setVisibility(View.VISIBLE);
                btnRestartGame.setEnabled(false);
                startGame();
            }
        }else if(v.getId() == btnRestartGame.getId()){
            txtSequence.setVisibility(View.VISIBLE);
            txtSequence.setText("Choose dificulty:");
            btnDifficultyEasy.setVisibility(View.VISIBLE);
            btnDifficultyHard.setVisibility(View.VISIBLE);
            btnRestartGame.setEnabled(false);

        }else if(v.getId() == btnDifficultyEasy.getId()){
            difficulty = "easy";
            txtSequence.setVisibility(View.INVISIBLE);
            btnDifficultyEasy.setVisibility(View.INVISIBLE);
            btnDifficultyHard.setVisibility(View.INVISIBLE);
            btnRestartGame.setEnabled(true);
            restartGame();
        }else if(v.getId() == btnDifficultyHard.getId()){
            difficulty = "hard";
            txtSequence.setVisibility(View.INVISIBLE);
            btnDifficultyEasy.setVisibility(View.INVISIBLE);
            btnDifficultyHard.setVisibility(View.INVISIBLE);
            btnRestartGame.setEnabled(true);
            restartGame();
        }
    }

    private void restartGame() {
        //send to particle
        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {
            @Override
            public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {

                List<String> functionParameters = new ArrayList<String>();
                command = "";
                commandS = "";
                for (int i = 0; i < 4; i++){
                    Random randomGenerator = new Random();
                    int rand = randomGenerator.nextInt(5 - 1) + 1;
                    if(i == 3){
                        command += rand;
                    }else {
                        command += rand + ",";
                    }


                    switch (rand){
                        case 1: commandS += "Red";
                            break;
                        case 2: commandS += "Blue";
                            break;
                        case 3: commandS += "Yellow";
                            break;
                        case 4: commandS += "Green";
                            break;
                        default:
                    }
                    if (i != 3){
                        commandS += " > ";
                    }

                }
                if(difficulty.equals("easy")){command += ",0";}else if(difficulty.equals("hard")){command += ",1";}
                Log.d(TAG, "command: " + command);
                functionParameters.add(command);

                try {
                    mDevice.callFunction("restartG", functionParameters);
                } catch (ParticleDevice.FunctionDoesNotExistException e) {
                    e.printStackTrace();
                }

                return -1;
            }

            @Override
            public void onSuccess(Object o) {
                Log.d(TAG, "Sent colors command to device.");
            }

            @Override
            public void onFailure(ParticleCloudException exception) {
                Log.d(TAG, exception.getBestMessage());
            }
        });

    }

    private void startGame() {


        if (mDevice == null) {
            Log.d(TAG, "Cannot find the device");
            return;
        }

        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {
            @Override
            public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {
                subscriptionId = ParticleCloudSDK.getCloud().subscribeToAllEvents(
                        "broadcastMessage",  // the first argument, "eventNamePrefix", is optional
                        new ParticleEventHandler() {
                            public void onEvent(String eventName, ParticleEvent event) {
                                Log.i(TAG, "Received event with payload: " + event.dataPayload);
                                Log.i(TAG, "deviceId: " + event.deviceId);
                                Thread thread = new Thread() {
                                    @Override
                                    public void run() {
                                        try { Thread.sleep(1000); }
                                        catch (InterruptedException e) {}

                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {

                                                if (event.dataPayload.equals("Win")){
                                                    txtSequence.setText("Sequence" +
                                                            (difficulty.equals("easy")? "(Easy): ":
                                                                    difficulty.equals("easy")? "(Hard): ":
                                                                            "(Easy): ")
                                                            + commandS);
                                                    txtSequence.setVisibility(View.VISIBLE);
                                                    Toast.makeText(getApplicationContext(), "You Win!", Toast.LENGTH_LONG).show();
                                                }else if(event.dataPayload.equals("Lost")){
                                                    txtSequence.setText("Sequence" +
                                                            (difficulty.equals("easy")? "(Easy): ":
                                                                    difficulty.equals("easy")? "(Hard): ":
                                                                            "(Easy): ")
                                                            + commandS);
                                                    txtSequence.setVisibility(View.VISIBLE);
                                                    Toast.makeText(getApplicationContext(), "You Lost!", Toast.LENGTH_LONG).show();
                                                }else if(event.dataPayload.equals("Restart")){
                                                    txtSequence.setVisibility(View.VISIBLE);
                                                    txtSequence.setText("Choose difficulty:");
                                                    btnDifficultyEasy.setVisibility(View.VISIBLE);
                                                    btnDifficultyHard.setVisibility(View.VISIBLE);
                                                    btnRestartGame.setEnabled(false);
                                                }
                                            }
                                        });
                                    }
                                };
                                thread.start();


                            }

                            public void onEventError(Exception e) {
                                Log.e(TAG, "Event error: ", e);
                            }
                        });
                return -1;
            }

            @Override
            public void onSuccess(Object o) {
                Log.d(TAG, "Successfully got device from Cloud");
            }

            @Override
            public void onFailure(ParticleCloudException exception) {
                Log.d(TAG, exception.getBestMessage());
            }
        });
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
